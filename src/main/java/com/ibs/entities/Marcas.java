package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the marcas database table.
 * 
 */
@Entity
@Table(name="marcas")
public class Marcas implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 25 )
	@NotBlank(message = "marca no puede ir vacio")		
	private String marca;

	public Marcas() {
	}
	
	public Marcas(String marca) {
		this.marca = marca;
	}
	
	public Marcas(Long id, String marca) {
		this.id = id;
		this.marca = marca;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public static Marcas fromId(Long id) {
	  Marcas entity = new Marcas();
      entity.id = id;
      return entity;
    }

}