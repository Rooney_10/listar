package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Productos;

@Repository
public interface ProductosRepository extends CrudRepository<Productos, Long> {

}
