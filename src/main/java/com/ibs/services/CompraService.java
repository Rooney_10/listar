package com.ibs.services;

import java.util.List;

import javax.transaction.Transactional;

import com.ibs.entities.Compras;
import com.ibs.entities.Productos;
import com.ibs.entities.Proveedores;
import com.ibs.entities.Usuarios;
import com.ibs.repository.ComprasRepository;
import com.ibs.repository.ProductoRepository;
import com.ibs.repository.ProveedoresRepository;
import com.ibs.repository.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * CompraService
 */
@Service
public class CompraService {

    @Autowired
    ProductoRepository rProducto;

    @Autowired
    ProveedoresRepository rProveedor;

    @Autowired
    UsuarioRepository rUser;

    @Autowired
    ComprasRepository rCompra;

    @Transactional
    public List<Compras> getAllCompra() {
        return (List<Compras>) rCompra.findAll();
    }

    @Transactional
    public Boolean save(Compras entity) {
        try {
            rCompra.save(entity);
            return true;
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            return false;
        }
    }

    @Transactional
    public List<Proveedores> getAllProveedores() {
        return (List<Proveedores>) rProveedor.findAll();
    }

    @Transactional
    public List<Productos> getAllProductos() {
        return (List<Productos>) rProducto.findAll();
    }

    @Transactional
    public Proveedores getProveedores(Long id) {
        return rProveedor.findById(id).get();
    }

    @Transactional
    public Usuarios getUsuario(Long id) {
        return rUser.findById(id).get();
    }

    @Transactional
    public Productos getProducto(Long id) {
        return rProducto.findById(id).get();
    }

}