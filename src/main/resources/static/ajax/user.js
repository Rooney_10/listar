$(document).ready(inicio);
const api = "/usuario/";

//busca_id
function cargarRegistro(id) {
	$('.btnload').text('Modificar');
	$('.tituloModal').text('Modificar Registro');
	$.ajax({
		url : api + id,
		method : "post",
		success : function(response) {
			$("#id").val(response.id);
			$("#username").val(response.username);
			$("#password").val(response.password);
			$("#correo").val(response.correo);
			$("#direccion").val(response.direccion);
			$("#dui").val(response.dui);
			$("#telefono").val(response.telefono);
			$("#idRol").val(response.idRol);
			
			//llenar el modal eliminars
			$("#dato").text(response.username);
			setId(response.id);
		},
		error : function(response) {
			alert("Error al obtener el dato");
		}
	});
}

//funcion_listar
function cargarDatos() {
    $("#tablaConsultas").DataTable({

        "ajax": {
            "url": api,
            "method": "post"
        },
        "columns": [/* {
                "data": "id",
                "width": "5%"
            }, */
            {
                "data": "username",
                "width": "20%"
            },
            {
            	"data": "correo",
            	"width": "20%"
            },
            {
                "data": "password",
                "width": "20%"
            },
            {
            	"data": "direccion",
            	"width": "20%"
            },
            {
            	"data": "dui",
            	"width": "20%"
            },
            {
            	"data": "telefono",
            	"width": "20%"
            },
            {
            	"data": "rol",
            	"width": "20%"
            },
            {
                "data": "operacion",
                "width": "5%"
            }
        ],
        
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}
